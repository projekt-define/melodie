import numpy
import pygame

sampleRate = 44100
freq = 440

# notes
_c3=130.81
_cs3=138.59
_d3=146.83
_ds3=155.56
_e3=164.81
_f3=174.61
_fs3=185
_g3=196
_gs3=207.65
_a3=220
_as3=233.08
_b3=246.94

_c4=261.63
_cs4=277.18
_d4=293.66
_ds4=311.13
_e4=329.63
_f4=349.23
_fs4=369.99
_g4=392
_gs4=415.30
_a4=440
_as4=466.16
_b4=493.88

def tone(freq=freq,duration=1000):
    "play tone"
    arr = numpy.array(
        [4096 * numpy.sin(2.0 * numpy.pi * freq * x / sampleRate) for x in range(0, sampleRate)]
    ).astype(numpy.int16)
    arr2 = numpy.c_[arr,arr]
    sound = pygame.sndarray.make_sound(arr2)
    sound.play(-1)
    fadeout=50
    pygame.time.delay(duration-fadeout)
    sound.fadeout(fadeout)
    #sound.stop()

def melody(freq_n_dur=[[0,0],[0,0]]):
    "play melody, list of list with frequency and duration in ms"
    pygame.mixer.init(44100,-16,2,512)
    arr=[]
    duration=0
    for i in freq_n_dur:
        arr+=[4096
              * numpy.sin(2.0 * numpy.pi * i[0] * x / sampleRate)
              * numpy.sin(numpy.pi * x / (sampleRate * i[1] / 1000))
              for x in range(0, int(sampleRate * i[1] / 1000))]
        duration+=i[1]

    arr = numpy.array(arr).astype(numpy.int16)
    arr2 = numpy.c_[arr,arr]
    sound = pygame.sndarray.make_sound(arr2)
    sound.play(-1)
    fadeout=50
    pygame.time.delay(duration-fadeout)
    sound.fadeout(fadeout)

melody([[_e3,500],
        [_f3,500],
        [_fs3,500],
        [_g3,500],
        [_gs3,500],
        [_a3,500],
        [_as3,500],
        [_b3,500],
        [_c4,500],
        [_cs4,500],
        [_d4,500],
        [_ds4,500],
        [_e4,500],
        [_f4,500],
        [_fs4,500],
        [_g4,500],
        [_gs4,500],
        [_a4,500],
        [_as4,500],
        [_b4,500]
        ])

# pygame.mixer.init(44100,-16,2,512)
# # sampling frequency, size, channels, buffer

# # Sampling frequency
# # Analog audio is recorded by sampling it 44,100 times per second, 
# # and then these samples are used to reconstruct the audio signal 
# # when playing it back.

# # size
# # The size argument represents how many bits are used for each 
# # audio sample. If the value is negative then signed sample 
# # values will be used.

# # channels
# # 1 = mono, 2 = stereo

# # buffer
# # The buffer argument controls the number of internal samples 
# # used in the sound mixer. It can be lowered to reduce latency, 
# # but sound dropout may occur. It can be raised to larger values
# # to ensure playback never skips, but it will impose latency on sound playback. 

# arr = numpy.array([4096 * numpy.sin(2.0 * numpy.pi * freq * x / sampleRate) for x in range(0, sampleRate)]).astype(numpy.int16)
# arr2 = numpy.c_[arr,arr]
# sound = pygame.sndarray.make_sound(arr2)
# sound.play(-1)
# pygame.time.delay(1000)
# sound.stop()
