# Melodie
Spiele Töne mit [Python3](https://www.python.org/).

<https://gitlab.opensourceecology.de/define/melodie>

# Vorraussetzungen
Benötigt die Bibliotheken [NumPy](https://numpy.org/) und [pygame](https://www.pygame.org/news).

Installation der benötigten Packete mit [pip](https://pypi.org/project/pip/) dem Packetverwaltungsprogramm für Python Packete.

~~~
pip install numpy

pip install pygame
~~~


# Kontakt
[https://defineblind.at](https://defineblind.at)

[Mail: Info Define info_at_defineblind.at](mailto:Info Define <info_at_defineblind.at>)

# Namensnennung
BSVWNB/Define

[https://defineblind.at](https://defineblind.at)

# Anerkennung
Diese Arbeit wurde im Rahmen des Projekts [Define](https://defineblind.at), [Mail: Info Define info_at_defineblind.at](mailto:Info Define <info_at_defineblind.at>) entwickelt.

Die Trägerorganisation für das Define-Projekt ist der [Blinden- und Sehbehindertenverband Wien, Niederösterreich und Burgenland](https://www.blindenverband-wnb.at/) (BSVWNB). 

Dieses Projekt wird im Rahmen des [Digitalisierungsfonds Arbeit 4.0 von der AK Wien gefördert](https://wien.arbeiterkammer.at/service/digifonds/index.html).

Johannes Strelka-Petz übertrug das C-Programm Melody von Tom Igoe nach Python3.

Melodie beruht auf der Arbeit von

Tom Igoe, 2011, public domain

<https://www.arduino.cc/en/Tutorial/Tone>

# Urheberrecht & Nutzungsrechte
    SPDX-FileCopyrightText: 2023 Johannes Strelka-Petz <johannes_at_oskars.org> for BSVWNB/Define
    SPDX-License-Identifier: CERN-OHL-S-2.0+
    ------------------------------------------------------------------------------
    | Copyright 2023 Johannes Strelka-Petz for BSVWNB/Define                       |
    |                                                                              |
    | This source describes Open Hardware and is licensed under the CERN-OHL-S v2  |
    | or any later version.                                                        |
    |                                                                              |
    | You may redistribute and modify this source and make products using it under |
    | the terms of the CERN-OHL-S v2 or any later version                          |
    | (https://ohwr.org/cern_ohl_s_v2.txt).                                        |
    |                                                                              |
    | This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
    | INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
    | PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 or any later version        |
    | for applicable conditions.                                                   |
    |                                                                              |
    | Source location: https://gitlab.com/teamoskar/oskar_dib_pcb                  |
    |                                                                              |
    | As per CERN-OHL-S v2 section 4, should You produce hardware based on this    |
    | source, You must where practicable maintain the Source Location visible      |
    | on the Oskar Zither PCB or other products you make using this source.        |
     ------------------------------------------------------------------------------

